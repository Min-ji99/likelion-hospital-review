package com.likelion.hospitalreview.controller;

import com.likelion.hospitalreview.domain.dto.UserJoinRequest;
import com.likelion.hospitalreview.domain.dto.UserJoinResponse;
import com.likelion.hospitalreview.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
@Slf4j
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/join")
    public ResponseEntity<UserJoinResponse> join(@RequestBody UserJoinRequest dto){
        UserJoinResponse userJoinResponse=userService.join(dto);

        return ResponseEntity.ok().body(userJoinResponse);
    }
}

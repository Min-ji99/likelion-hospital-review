package com.likelion.hospitalreview.service;

import com.likelion.hospitalreview.domain.User;
import com.likelion.hospitalreview.domain.dto.UserJoinRequest;
import com.likelion.hospitalreview.domain.dto.UserJoinResponse;
import com.likelion.hospitalreview.exception.AppException;
import com.likelion.hospitalreview.exception.ErrorCode;
import com.likelion.hospitalreview.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    public UserService(UserRepository userRepository, BCryptPasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    public UserJoinResponse join(UserJoinRequest userJoinRequest) {
        userRepository.findByUserName(userJoinRequest.getUserName())
                .ifPresent(user->{
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME, String.format("존재하는 이름입니다."));
                });
        User user=userRepository.save(userJoinRequest.toEntity(encoder.encode(userJoinRequest.getPassword())));
        log.info(user.getUserName());
        return UserJoinResponse.builder()
                .userName(user.getUserName())
                .message("회원가입에 성공했습니다.")
                .build();
    }
}

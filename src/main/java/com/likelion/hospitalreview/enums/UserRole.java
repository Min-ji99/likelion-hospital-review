package com.likelion.hospitalreview.enums;

public enum UserRole {
    ADMIN,
    USER
}
